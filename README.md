# Formation Docker: session "intermediaire"

> Nous vous recommandons de bien lire l'ensemble de la consigne avant de démarrer un exercice.

## Exercice 01

### Docker Compose

Vous allez devoir lancer trois conteneurs sur votre machine en même temps via [docker compose](https://docs.docker.com/compose/) : [wordpress](https://wordpress.com/fr/), [mysql](https://www.mysql.com/fr/) et [adminer](https://www.adminer.org/)

Pour chacun d'entre eux, vous utiliserez les [mots-clés docker compsoe](https://docs.docker.com/compose/compose-file/05-services/) suivant :

```text
image
restart
ports (si nécessaire)
environment (si nécessaire)
volume (si nécessaire)
depends_on
container_name
```

Pour ce qui est des des ports d’écoute, vous devriez réussir à les identifier vous même :p

Pour me faire pardonner quelques commandes :

- `docker compose up` : pour tout construire 
- `docker container ps` : pour voir le status des conteneurs
- `docker compose down` : pour tout détruire

Plus de précisions et de paramétrage sur ces commandes dans les diapositives ;-)

En plus des conteneurs, vous devrez ajouter à vos conteneurs **Wordpress** et **MySQL** des volumes **à la volée**. Pour rappel, ce sont des volumes correspondant à un dossier de votre sytème hôte.

Une fois tout en place, afficher votre site web dans un onglet, puis dans un autre votre [adminer](https://www.adminer.org/). Sur [adminer](https://www.adminer.org/), éditer le nom de votre blog, en modifiant l'`option_value` de l'`option_name` : `blogname` situé dans la table `wp_options`.

Retourner sur votre site web et actualisr la page pour constater les changements !

Vous pouvez également détruire et reconstruire vos conteneurs pour vous assurer du bon fonctionnement des volumes.

### Questions pour cet exercice

1. Quel est le mot de passe généré aléatoirement par Docker ?

Tip : Utilisez la commande `docker container logs` sur le conteneur `mysql`afin de récupérer le mot de passe root qui a été généré aléatoirement.

2. A l'aide des slides et de la documentation en ligne de Docker, utilisez la directive `include` de manière à réaliser un déploiement qui se compose d'un service de base de données (MySQL) et d'un service web (pour WordPress). 

La définition de chaque service devra être réalisée dans un sous-répertoire du répertoire principal nommé `deploiement`.

## Exercice 02 : Volumes en amont

1 - Créer un volume en amont  
2 - A l'aide d'un conteneur Ubuntu, remplir ce volume en amont avec un ou plusisuers fichiers/dossiers   
3 - A l'aide des slides, sauvegarder le contenu de ce volume en amont dans une archive  
4 - Vérifier le contenu de cette archive  
5 - Créer un second volume en amont  
6 - A l'aide d'un conteneur Debian, remplir ce second volume en amont avec le contenu de l'archive

## Exercice 03 : Multi-Stage

### Facile

~~Réaliser un site statique avec [Hugo](https://gohugo.io), il vous faudra appliquer un thème et générer au moins un article.~~

~~Pour cela la [documentation](https://gohugo.io/getting-started/quick-start/) devrait vous être très utile.~~

Votre site a été généré dans le dossier `public` avec la commande `hugo`, vérifier la présence du fichier `index.html`.

Supprimer le dossier `public`, vous n'en aurez pas besoin puisque vous allez le reconstruire dans le multi-stage.

Écrire un `Dockerfile` multi-stage pour générer votre et héberger votre site dans un conteneur Docker avec un serveur web.

N'hésitez pas à vous inspirer des diapositives pour l'écriture du Dockerfile.

### Difficile

Réaliser un Dockerfile Multi-Stage avec [Pandoc](https://pandoc.org/) pour cela il vous faudra :

- [ ] Identifier votre image source depuis le [DockerHub](https://hub.docker.com) et l'initialiser si vous avez besoin de faire des installations dessus.
- [ ] Copier sur votre image un fichier texte (docx, rtf, txt, md, ...) de votre choix et convertissez le en HTML. Jetez un oeil aux [démos](https://pandoc.org/demos.html)
- [ ] Récupérer le fichier HTML ainsi générer et exposer le via une image NGINX

## Exercice 04 : Interface pour Docker avec Portainer

Réalisez le déploiement du service Portainer à l'aide de Docker en utilisant une stack `compose`. 

Utilisez un volume (`bind` ou `volume`) pour garder la persistance des données (notamment pour les informations du compte administrateur).
